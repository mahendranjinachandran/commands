#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>

void main()
{
    #pragma omp parallel
    {  

        clock_t t;

        //start_time of the thread to run
        t = clock();

        printf("Hello World....from thread=%d\n",omp_get_thread_num());

        //end_time of the thread to finish
        t = clock()-t;
        double time_taken = ((double)t)/CLOCKS_PER_SEC;

        //Total time taken
        printf("Time taken:%f -> %d\n",time_taken,omp_get_thread_num());

    }
}



/*
The code inside the pragma omp paralled runs in a parallelised way
export OMP_NUM_THREADS=5 -> Specifies the number of threads to run throught CMD
To compile -> gcc -o hello -fopenmp hello.c
To run -> ./hello

*/